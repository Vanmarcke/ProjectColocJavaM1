package projectcoloc;

import java.util.ArrayList;
import static projectcoloc.ProjectColoc.write;

/** The Owner class, witch is a subclass of Human
 *
 * @author AFK
 */
public class Owner extends Human implements Adults_Actions {

    /** Value claimed by the owner to other objects (roomates, guests)
     *
     */
    public int money_claimed;

    /** The Owner constructor, with a firstname, name and sexe
     * And money_claimed initialized at 0
     * @param surnom
     * @param prenom
     * @param sexe
     */
    public Owner(String surnom, String prenom, String sexe)
    {
        super(surnom,prenom,sexe);
        money_claimed = 0;
    }
    
    /** The owner can claim a money account to roomates
     *  This gets her angry
     * @param n
     */
    public void claim(int n) {
       this.knock();
       this.talk("You little bastards didn't pay your rent ! Did you ? \nYou owe me "+Integer.toString(n)+"€.");
       this.rage_state++;
    }

    /** The owner can knock on the roomates door 
     * It's never a good thing, this method causes other problems for roomates
     */
    public void knock() {
        if(this.rage_state == 0)
        {
            write("The owner "+this.first_name+" knocks on the door...",true);
            this.rage_state++;
        }
        else if(this.rage_state == 1 || this.rage_state == 2 || this.rage_state == 3)
        {
            write("The owner "+this.first_name+" knocks on the door... again!",true);
            this.rage_state++;
        }
        else
        {
            this.stop_party();
        }
    }
    
    /**The owner can stop the party : the boolean is set to 'true'
     * 
     */
    public void stop_party() {
        write("WHAT IS THIS MESS ?? EVERYONE OUT NOW OR I WILL CALL THE CUPS\nThe owner "+this.first_name+" ends the party.",true);
        ProjectColoc.end = true;
    }
    
    /** Method that returns an ArrayList with all the Owners of a list (human_list for example)
     *
     * @param list
     * @return
     */
    public static ArrayList get_owner_list(ArrayList list)
    {
        ArrayList owner_list = new ArrayList();
        for(int i = 0;i<list.size();i++)
        {
            if(list.get(i) instanceof Owner)
            {
                owner_list.add(list.get(i));
                owner_list.add(i);
            }
        }
        return(owner_list);
    }
}
