/*
 * JAVA PROJECT: PROJECTCOLOC
 * by Romain VANMARCKE & Adrien BOURGOIN
 */
package projectcoloc;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.lang.Thread.sleep;



/**
 * The main class of the project
 * @author Romain
 */
public class ProjectColoc {
    
    /** 
     * A boolean that tell us that the party was finished for some reasons
     */
    public static Boolean end = false;
    /** 
     * A long that is the parameter of the sleep function between two events
     */
    private static long sleep_time = 3000;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Object> dead_list = new ArrayList();
        //CREATION GUESTS
        
        ArrayList<Guest> guest_list = new ArrayList();
        Guest guest1 = new Guest("Poil de carottes","Quentin","Man",100,Music.RAP);
        Guest guest2 = new Guest("La fêtarde","Julie","Woman",100,Music.TECHNO);
        Guest guest3 = new Guest("La morfale","Margaux","W",100,Music.CLASSIC);
        guest_list.add(guest1);
        guest_list.add(guest2);
        guest_list.add(guest3);
        
        //CREATION ROOMATES
        
        ArrayList<Roomate> roomate_list = new ArrayList();
        Roomate coloc1 = new Roomate("Michmich","Michel","Man",100,Music.GOSPEL,roomate_list.size()+1);
        Roomate coloc2 = new Roomate("Jojo","Joël","Man",100,Music.TECHNO,roomate_list.size()+2);
        Roomate coloc3 = new Roomate("La coquine","Clara","Woman",100,Music.CLASSIC,roomate_list.size()+3);
        roomate_list.add(coloc1);
        roomate_list.add(coloc2);
        roomate_list.add(coloc3);
        
        //CREATION NEIGHBOURS
        
        ArrayList<Neighbour> neighbour_list =  new ArrayList();
        Neighbour voisin1 = new Neighbour("Victime","Patrick","Man");
        neighbour_list.add(voisin1);
        
        //CREATION PROPRIO
        
        Owner proprio = new Owner("Mémé","Jacqueline","W");
        
        //CREATION ANIMALS
     
        ArrayList<Cat> cat_list = new ArrayList();
        ArrayList<Lion> lion_list = new ArrayList();
        ArrayList<Monkey> monkey_list = new ArrayList();
        Cat chat1 = new Cat("Garfield");
        Cat chat2 = new Cat("Gribouille");
        Lion lion1 = new Lion("Aslan");
        Lion lion2 = new Lion("Scar");
        Monkey singe1 = new Monkey("Cheeta");
        Monkey singe2 = new Monkey("King Kong");
        cat_list.add(chat1);
        cat_list.add(chat2);
        lion_list.add(lion1);
        lion_list.add(lion2);
        monkey_list.add(singe1);
        monkey_list.add(singe2);
        ArrayList<Human> teenagers_list = new ArrayList();
        teenagers_list.addAll(guest_list);
        teenagers_list.addAll(roomate_list);
        ArrayList<Human> boring_people_list = new ArrayList();
        boring_people_list.addAll(neighbour_list);
        boring_people_list.add(proprio);
        ArrayList<Animal> animal_list = new ArrayList();
        animal_list.addAll(cat_list);
        animal_list.addAll(lion_list);
        animal_list.addAll(monkey_list);
        
        //RECAPITULATIF
        write("",false); // 
        recap(teenagers_list,boring_people_list,animal_list,dead_list);
        
        //CHOICE : ADD OTHER MEMBERS TO THE PARTY
        choice(teenagers_list,boring_people_list,animal_list);
        write("",false);
        recap(teenagers_list,boring_people_list,animal_list, dead_list);  

        //GENERATION EVENT 
        
        //TIME GENERATION
        Date time = new Date(); //get current time
        time.setHours(20); // make it starts after 20:00
        SimpleDateFormat ft = new SimpleDateFormat ("HH:mm"); // Set the correct display of the time
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        String newTime;
        
        //Min and max values for the time-lapse between two events
        int max=20,min=60;
        for(int i=0;i<10; i++) {  // Generate 10 events
            if (!end) {
                int nbAlea = (int)(Math.random()*(max-min))+min;
                cal.add(Calendar.MINUTE, nbAlea); // add a random amount of time between min and max minutes
                newTime = ft.format(cal.getTime());
                // MUSIC GENERATION
                int nb = (int)(Math.random()*6);
                Music music ;
                switch(nb){
                    case 1: 
                        music = Music.CLASSIC;
                        break;
                    case 2:
                        music = Music.GOSPEL;
                        break;
                    case 3:
                        music = Music.RAP;
                        break;
                    case 4:
                        music = Music.ROCK;
                        break;
                    case 5:
                        music = Music.TECHNO;
                        break;
                    default:
                        music = Music.NO_MUSIC;
                        break;
                }                   
                write("\n"+newTime+": "+music,true);
                // Call the main method that will create some events
                create_events(music,animal_list,teenagers_list,boring_people_list,dead_list); 
                write("\n--------------------------------------------------------------------------",true);
                // SLEEP between each event
                try {
                    sleep(sleep_time);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ProjectColoc.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        write("\n",true);
        // Declare the end of the party
        ProjectColoc.end = true; 
        // Do a final Recap
        recap(teenagers_list,boring_people_list,animal_list,dead_list);
        
    }
    
    // START OF THE METHODS

    /**
     * The start point for our events
     * For each event, it creates a random number of actions & scans the triggers of special events
     * @param music 
     * @param animal_list
     * @param teenagers_list
     * @param boring_people_list
     * @param dead_list
     */
    public static void create_events(Music music, ArrayList animal_list, ArrayList teenagers_list, ArrayList boring_people_list, ArrayList dead_list) {
        Integer modifier = 3;
        // Generate a random number between 0 and 10 (add to it the modifier value)
        Integer pb = (int)(Math.random()*10 + modifier); 
        int counter = 0;
        // While the random value pb is superior to 3, create a new action
        while((pb > 3) && (ProjectColoc.end == false)) {  
            counter += 1;
            // If the students are not all dead, create one action
            if (!teenagers_list.isEmpty()) {
                generer_action(counter, animal_list,teenagers_list,boring_people_list,dead_list);
            }
            // Else end the party
            else {
                ProjectColoc.end = true;
                write("END OF THE PARTY\nThe students all died tonight !",true);
            }
            // decrease the modifier to lower the chance to have a new action in the next turn
            modifier--;
            // generate a new random value
            pb = (int)(Math.random()*10*modifier);
            
        }
        // If the party has not ended in the last actions, scan the triggers' events
        if (ProjectColoc.end == false) {
            scan(teenagers_list, boring_people_list, animal_list, music, dead_list); 
        }
        
        
    }
    
    /**
     * Generate an action with the following scheme:
     * 1st action: pick two students and make them perform a random action
     * 2nd action: pick one student and one animal...
     * 3rd action: pick one student and one adult people...
     * 4th action: pick two students again...
     * 5th action: pick two animals...
     * 6th action: back to the 1st action
     * ...........
     * @param counter
     * @param animal_list
     * @param teenagers_list
     * @param boring_people_list
     * @param dead_list
     */
    public static void generer_action(int counter, ArrayList animal_list, ArrayList teenagers_list, ArrayList boring_people_list, ArrayList dead_list) {
        Integer pick1;
        Integer pick2;
        switch(counter%5) {
            //1st action: pick two students and make them perform a random action
            case 1:
                pick1 = (int)(Math.random()*teenagers_list.size()); 
                pick2 = (int)(Math.random()*teenagers_list.size());
                Human ind1 = (Human) teenagers_list.get(pick1);
                Human ind2 = (Human) teenagers_list.get(pick2);
                // Guest Action
                if(ind1 instanceof Guest) {
                    Guest guest = (Guest) ind1;
                    pick1 = (int)(Math.random()*6); 
                    switch(pick1) {
                        case 0:
                            guest.have_sex(ind2);
                            break;
                        case 1:
                            guest.break_object(teenagers_list);
                            break;
                        case 2:
                            guest.fightswith(ind2,dead_list,teenagers_list);
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        default:
                            guest.drink();
                            break;
                    }
                }
                // Roomate Action
                else {
                    Roomate roomate = (Roomate) ind1;
                    pick1 = (int)(Math.random()*6); 
                    switch(pick1) {
                        case 0:
                            roomate.have_sex(ind2);
                            break;
                        case 1:
                            roomate.pee(ind2);
                            break;
                        case 2:
                            roomate.fightswith(ind2,dead_list,teenagers_list);
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        default:
                            roomate.drink();
                            break;
                    }
                }
                break;
            //2nd action: pick one student and one animal...
            case 2:
                pick1 = (int)(Math.random()*teenagers_list.size()); 
                pick2 = (int)(Math.random()*animal_list.size());
                Human ind3 = (Human) teenagers_list.get(pick1);
                Animal ind4 = (Animal) animal_list.get(pick2);
                // Monkey Action
                if(ind4 instanceof Monkey) {
                    Monkey monkey = (Monkey) ind4;
                    pick1 = (int)(Math.random()*10); 
                    switch(pick1) {
                        case 0:
                            monkey.have_sex(ind3);
                            break;
                        case 1:
                            monkey.pee(ind3);
                            break;
                        case 2:
                            monkey.fights(ind3);
                            ind3.is_dead(dead_list,teenagers_list);
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        case 4:
                            monkey.steal(ind3);
                            break;
                        default:
                            monkey.drink();
                            break;
                    }
                }
                // Cat action
                else if (ind4 instanceof Cat){
                    Cat cat = (Cat) ind4;
                    pick1 = (int)(Math.random()*6); 
                    switch(pick1) {
                        case 0:
                            cat.scratch(ind3);
                            ind3.is_dead(dead_list,teenagers_list);
                            break;
                        case 1:
                            cat.defecate();
                            break;
                        case 2:
                            cat.eat();
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        default:
                            cat.scratch(ind3);
                            ind3.is_dead(dead_list,teenagers_list,boring_people_list);
                            break;
                    }
                }
                // Lion action
                else {
                    Lion lion = (Lion) ind4;
                    pick1 = (int)(Math.random()*6); 
                    switch(pick1) {
                        case 0:
                            lion.roar();
                            break;
                        case 1:
                            lion.defecate();
                            break;
                        case 2:
                            lion.devour(ind3);
                            ind3.is_dead(dead_list,teenagers_list);
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        default:
                            lion.roar();
                            break;
                    }
                }
                break;
            // 3rd action: pick one student and one adult...
            case 3:
                pick1 = (int)(Math.random()*teenagers_list.size()); 
                pick2 = (int)(Math.random()*boring_people_list.size());
                Human ind5 = (Human) teenagers_list.get(pick1);
                Human ind6 = (Human) boring_people_list.get(pick2);
                // Neighbour Action
                if(ind6 instanceof Neighbour) {
                    Neighbour neighbour = (Neighbour) ind6;
                    int o = (Integer)Owner.get_owner_list(boring_people_list).get(1);
                    pick1 = (int)(Math.random()*6); 
                    switch(pick1) {
                        case 0:
                            neighbour.knock();
                            neighbour.insult(ind5);
                            break;
                        case 1:
                            neighbour.complain((Owner)boring_people_list.get(o),teenagers_list);
                            break;
                        case 2:
                            neighbour.knock();
                            neighbour.fightswith(ind5,dead_list,teenagers_list);
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        default:
                            neighbour.complain((Owner)boring_people_list.get(o),teenagers_list);
                            break;
                    }
                }
                // Owner Action
                else {
                    Owner owner = (Owner) ind6;
                    pick1 = (int)(Math.random()*8); 
                    switch(pick1) {
                        case 0:                            
                            owner.stop_party();
                            break;
                        case 1:
                            owner.claim(500);
                            if (ind5 instanceof Roomate) {
                                ind5.jump_out_of_window(dead_list,teenagers_list);
                            }
                            else {
                                ind5.insult(owner);
                            }
                            break;
                        case 2:
                            owner.knock();
                            owner.fightswith(ind5,dead_list,teenagers_list);
                            break;
                        case 3:
                            ProjectColoc.all_drink(teenagers_list);
                        default:
                            owner.knock();
                            owner.insult(ind5);
                            up_enervement(teenagers_list);
                            break;
                    }
                }
                break;
            //4th action: pick two students again...    
            case 4:
                pick1 = (int)(Math.random()*teenagers_list.size()); 
                pick2 = (int)(Math.random()*teenagers_list.size());
                Human ind7 = (Human) teenagers_list.get(pick1);
                Human ind8 = (Human) teenagers_list.get(pick2);
                // Guest action
                if(ind7 instanceof Guest) {
                    Guest guest = (Guest) ind7;
                    guest.drink();
                    guest.fightswith(ind8,dead_list,teenagers_list);
                    guest.have_sex(ind8);
                }
                // Roomate action
                else {
                    Roomate roomate = (Roomate) ind7;
                    roomate.drink();
                    roomate.fightswith(ind8,dead_list,teenagers_list);
                }
                break;
            //5th action: pick two animals...
            case 0:
                pick1 = (int)(Math.random()*animal_list.size()); 
                pick2 = (int)(Math.random()*animal_list.size());
                Animal ind9 = (Animal) animal_list.get(pick1);
                Animal ind10 = (Animal) animal_list.get(pick2);
                // Monkey action
                if(ind9 instanceof Monkey) {
                    Monkey monkey = (Monkey) ind9;
                    monkey.defecate();
                    if(ind10 instanceof Lion) {
                       Lion lion = (Lion)ind10;
                       lion.roar();
                    }
                }
                // Cat action
                else if (ind9 instanceof Cat){
                    Cat cat = (Cat) ind9;
                    cat.defecate();
                }
                // Lion action
                else {
                    Lion lion = (Lion) ind9;
                    lion.defecate();
                }
                break;
            default:
                write("You should not be able to see this, you've got an error with your decors'switch",true);
                break;
        }
        
    }
    
    /**
     * Write on the file
     * @param text the string to write on the .txt file
     * @param save a boolean, when true = append to the end of the file, when false = erase the file when writing
     */
    public static void write(String text, boolean save)
    {
        //Get the address of the file
        String adressedufichier = System.getProperty("user.dir") + "/" + "log.txt";
        try
        {
            // Create a new FileWrite on the file
            FileWriter fw = new FileWriter(adressedufichier, save);				
            // Create a new buffer
            BufferedWriter output = new BufferedWriter(fw);				
            // Add the text to the buffer
            output.write(text+"\n");
            // Add the buffer to the file 		 		
            output.flush();
            // Close the buffer			
            output.close();
        }
        // In case of exception print the StackTrace
        catch(IOException ioe){
            System.out.print("Error : ");
            ioe.printStackTrace();
        }
    }
    
    /**
     * The console description that is first printed to ask the user if he wants to add some guys to the party  
     * @param teenagers_list
     */
    public static void description(ArrayList teenagers_list, ArrayList boring_people_list, ArrayList animal_list)
    {
        System.out.println("There are actually "+teenagers_list.size()+" students that planned to come to the party,");
        System.out.println("We have also "+boring_people_list.size()+" adults and " + animal_list.size()+ " animals that may interfere with the party.");
        System.out.println("\nYou can add other members/neighbours/animals in the party :");
        System.out.println("Enter 1 to add a guest");
        System.out.println("Enter 2 to add a roomate");
        System.out.println("Enter 3 to add a neighbour");
        System.out.println("Enter 4 to add a cat");
        System.out.println("Enter 5 to add a lion");
        System.out.println("Enter 6 to add a monkey");
        System.out.println("Enter an other number to continue without adding");
    }
    
    /**
     * Prints a recap of everyone, Students, Adults, Animals and even the deads
     * @param teenagers_list
     * @param boring_people_list
     * @param animals_list
     * @param dead_list
     */
    public static void recap(ArrayList teenagers_list,ArrayList boring_people_list,ArrayList animals_list,ArrayList dead_list)
    {
        if(end) {
            write("Everyone came back home, let's recap who died tonight: ",true);
        }
        else {
            write("Tonight will be THE night ! Let's see who is coming and start the party :",true);
        }
        display_list_write(teenagers_list,"The teenagers are: "); 
        display_list_write(boring_people_list,"The boring people are : ");
        display_list_write(animals_list,"The animals are : ");
        if (end) {
            if(dead_list.isEmpty()) {
                write("Nobody died tonight, what a shame !",true);
            }
            else {
                display_list_write(dead_list,"The beloved ones, RIP");
            }
        }
        
    }
    
    /**
     * Give the choice to the user to add some guys to the party
     * @param teenagers_list
     * @param boring_people_list
     * @param animals_list
     */
    public static void choice(ArrayList teenagers_list,ArrayList boring_people_list,ArrayList animals_list)
    {
        int check = 0;
        while(check == 0)
        {
            ProjectColoc.description(teenagers_list,boring_people_list,animals_list);
            Scanner reader = new Scanner(System.in); 
            System.out.println("\nYour choice : ");
            while(!reader.hasNextInt()){ System.out.println("\nPlease enter a number");reader.next();}
            int response = reader.nextInt();
            switch (response)
            {
                case 1 :
                    Guest.generate(teenagers_list);
                    break;
                case 2 :
                    Roomate.generate(teenagers_list);
                    break;
                case 3 : 
                    Neighbour.generate(boring_people_list);
                    break;
                case 4 : 
                    Cat.generate(animals_list);
                    break;
                case 5 :
                    Lion.generate(animals_list);
                    break;
                case 6 :
                    Monkey.generate(animals_list);
                    break;
                default : 
                    check=1;
                    System.out.println("Time to start the party !");
                    String adressedufichier = System.getProperty("user.dir") + "\\" + "log.txt";
                    System.out.println("You can follow the party in "+adressedufichier);
            }
        }
    }
    
    /**
     * Display every object of a given list, preceded by an announcement
     * @param list
     * @param string the announcement
     */
    public static void display_list_write(ArrayList list,String string) {
        write(string,true);
        for (int n = 0; n<list.size();n++)
        {
            write(list.get(n).toString(),true);
        }
        write("",true);
    }
    
    /** This method generates ONE event in the party, depending on the differents triggers (drunk_state, rage_state, music played)
     * We first create an event depending on the rage_state, if nobody is angry we look at the drunk_state,
     * if nobody is drunk we look at the music played and if nobody dislikes the music played we generate a random event
     * @param teenagers_list
     * @param boring_people_list
     * @param animal_list
     * @param music
     * @param dead_list
     */
    public static void scan(ArrayList<Human> teenagers_list, ArrayList<Human> boring_people_list, ArrayList<Animal> animal_list, Music music, ArrayList dead_list)
    {
        ArrayList<Monkey> monkey_list = Monkey.get_monkey_list(animal_list);
        ArrayList<Object> total_list = new ArrayList();
        ArrayList<Human> human_list = new ArrayList();
        human_list.addAll(teenagers_list);
        human_list.addAll(boring_people_list);
        total_list.addAll(human_list);
        total_list.addAll(animal_list);
        int compteur = 0;
        int max_enervement;
        int indice_enervement;
        if(!teenagers_list.isEmpty())
        {
            max_enervement = ProjectColoc.Find_max_taux_enervement(teenagers_list).get(0);
            indice_enervement = ProjectColoc.Find_max_taux_enervement(teenagers_list).get(1);
        }
        else
        {
            max_enervement = 0;
            indice_enervement = 0;
        }
        Object victime = ProjectColoc.Findvictim(total_list);
        Object victime2 = ProjectColoc.Findvictim(human_list);
        switch(max_enervement) // SWITCH TO MAKE AN EVENT WITH THE MOST ANGRY (rage_state max)
        {
            case 5:
                switch(victime.getClass().getSimpleName())
                {
                    case("Monkey") : 
                        human_list.get(indice_enervement).kill((Monkey)victime);
                        ((Monkey)victime).is_dead(animal_list,monkey_list,total_list);
                        break;
                    case("Lion") :
                        human_list.get(indice_enervement).kill((Lion)victime);
                        ((Lion)victime).is_dead(animal_list,total_list);
                        break;
                    case("Cat") :
                        human_list.get(indice_enervement).kill((Cat)victime);
                        ((Cat)victime).is_dead(animal_list,total_list);
                        break;
                    case("Neighour"):
                        human_list.get(indice_enervement).kill((Neighbour)victime);
                        ((Neighbour)victime).is_dead(human_list,boring_people_list,total_list);
                        break;
                    case("Roomate"):
                        human_list.get(indice_enervement).kill((Roomate)victime);
                        ((Roomate)victime).is_dead(human_list,boring_people_list,total_list);
                        break;
                    case("Guest") : 
                        human_list.get(indice_enervement).kill((Guest)victime);
                        ((Guest)victime).is_dead(human_list,teenagers_list,total_list);
                        break;
                    case("Owner") :
                        ProjectColoc.write(human_list.get(indice_enervement).nickname+" would like to kill "+((Owner)victime).first_name+" but she is not here.", true);
                        break;
                }
                compteur = 1;
                human_list.get(indice_enervement).rage_state=1; // We set the rage_state to 1 for the human who has the max rage_state
                write(human_list.get(indice_enervement).first_name+" calmed down a bit.",true);
                break;
            case 4 : 
                switch(victime.getClass().getSimpleName())
                {
                    case("Monkey") : 
                        human_list.get(indice_enervement).fightswith(victime,dead_list,animal_list,teenagers_list,boring_people_list,total_list,human_list);
                        break;
                    case("Lion") :
                        human_list.get(indice_enervement).fightswith(victime,dead_list,animal_list,teenagers_list,boring_people_list,total_list,human_list);
                        break;
                    case("Cat") :
                        human_list.get(indice_enervement).fightswith(victime,dead_list,animal_list,teenagers_list,boring_people_list,total_list,human_list);
                        break;
                    case("Neighbour"):
                        human_list.get(indice_enervement).fightswith(victime,dead_list,animal_list,teenagers_list,boring_people_list,total_list,human_list);
                        break;
                    case("Roomate"):
                        human_list.get(indice_enervement).fightswith(victime,dead_list,animal_list,teenagers_list,boring_people_list,total_list,human_list);
                        break;
                    case("Guest") : 
                        human_list.get(indice_enervement).fightswith(victime,dead_list,animal_list,teenagers_list,boring_people_list,total_list,human_list);
                        break;
                    case("Owner") :
                        ProjectColoc.write(human_list.get(indice_enervement).nickname+" would like to fight with "+((Owner)victime).nickname+" but she is not here...", true);
                        human_list.get(indice_enervement).rage_state=1;
                        break;
                }
                human_list.get(indice_enervement).rage_state=1; // We set the rage_state to 1 for the human who has the max rage_state
                write(human_list.get(indice_enervement).first_name+" calmed down a bit.",true);
                compteur = 1;
                break;
            case 3 :
                switch(victime2.getClass().getSimpleName())
                {
                    case("Neighbour") : 
                        human_list.get(indice_enervement).insult((Neighbour)victime2);
                        break;
                    case("Roomate"):
                        human_list.get(indice_enervement).insult((Roomate)victime2);
                        break;
                    case("Guest") : 
                        human_list.get(indice_enervement).insult((Guest)victime2);
                        break;
                    case("Owner") :
                        ProjectColoc.write(human_list.get(indice_enervement).nickname+" is insulting "+((Owner)victime2).nickname+" but she is not here...", true);
                        break;
                }
                human_list.get(indice_enervement).rage_state=1; // We set the rage_state to 1 for the human who has the max rage_state
                write(human_list.get(indice_enervement).first_name+" calmed down a bit.",true);
                compteur = 1;
                break;
            default :
                ProjectColoc.write("Nobody is upset yet but it's only the beginning...", true);
        }
        // IF NOBODY IS ENOUGH ANGRY WE COMPARE THE DRUNK STATE TO GENERATE AN EVENT :
        int indice_alcoolemie;
        float max_alcoolemie;
        if(!teenagers_list.isEmpty())
        {
            indice_alcoolemie = ProjectColoc.Find_max_degre_alcoolemie(teenagers_list);
            max_alcoolemie = teenagers_list.get(indice_alcoolemie).drunk_state;
        }
        else
        {
            indice_alcoolemie = 0;
            max_alcoolemie = 0;
        }
        if(max_alcoolemie >= 0.9 && compteur ==0 && max_alcoolemie < 1)
        {
            teenagers_list.get(indice_alcoolemie).jump_out_of_window(dead_list,teenagers_list,human_list,total_list);
            compteur = 1;
        }
        else if(max_alcoolemie >= 0.7 && max_alcoolemie < 0.9 && compteur == 0)
        {
            switch(teenagers_list.get(indice_alcoolemie).getClass().getSimpleName())
            {
                case "Roomate" :
                    ((Roomate)teenagers_list.get(indice_alcoolemie)).pee(ProjectColoc.Findvictim(teenagers_list,animal_list));
                    break;
                    
                case "Guest" :
                    ((Guest)teenagers_list.get(indice_alcoolemie)).pee(ProjectColoc.Findvictim(teenagers_list,animal_list));
                    break;
            }
            compteur = 1;
        }
        else if(max_alcoolemie >= 0.5 && max_alcoolemie < 0.7 && compteur == 0)
        {
            teenagers_list.get(indice_alcoolemie).fightswith(ProjectColoc.Findvictim(teenagers_list,animal_list),dead_list,animal_list,teenagers_list,boring_people_list);
            compteur = 1;
        }
        else if(max_alcoolemie >= 1 && compteur == 0)
        {
            if(teenagers_list.get(indice_alcoolemie) instanceof Roomate)
            {
                ((Roomate)(teenagers_list.get(indice_alcoolemie))).ethylic_coma(dead_list,teenagers_list,total_list,human_list);
                compteur = 1;
            }
            else if(teenagers_list.get(indice_alcoolemie) instanceof Guest)
            {
                ((Guest)(teenagers_list.get(indice_alcoolemie))).ethylic_coma(dead_list,teenagers_list,total_list,human_list);
                compteur = 1;
            }
        }
        if(compteur == 0 && !teenagers_list.isEmpty())
        {
            ProjectColoc.write("Nobody is drunk yet, we are definitly not in Lille... ", true);
        }    
        // IF THERE ARE NO DRUNK PEOPLE WE COMPARE THE HUMANS WHO DISLIKE THE SONG PLAYED
        ArrayList<Human> dislikers;
        if(!teenagers_list.isEmpty())
        {
            dislikers = ProjectColoc.dislikers_music(music,teenagers_list);
        }
        else
        {
            dislikers = new ArrayList();
        }
        if(!dislikers.isEmpty() && music!=Music.NO_MUSIC && compteur == 0)
        {
            for (int r=0;r<dislikers.size();r++)
            {
                write(dislikers.get(r).first_name+" doesn't like the song, he becomes angry",true);
                dislikers.get(r).rage_state++;
            }
            compteur = 1;
        }
        else
        {
            if(music != Music.NO_MUSIC && compteur == 0 && !teenagers_list.isEmpty())
            {
                ProjectColoc.write("Nobody dislikes the song played. What an enjoyable party!", true);
            }
            else if(teenagers_list.isEmpty())
            {
                write("This is so calm now...",true);
            }
        }
        // IF THERE ARE NO MUSIC DISLIKERS, WE GENERATE AN OTHER EVENT :
       if(compteur == 0)
        {
            int pick1 = (int)(Math.random() * 4);
            switch(pick1)
            {
                case 0 :
                    if(!teenagers_list.isEmpty())
                    {
                        ((Human)(Findvictim(teenagers_list))).eat_potatoes();
                    }
                    break;
                case 1 : 
                    if(!teenagers_list.isEmpty())
                    {
                        ((Human)(Findvictim(teenagers_list))).talk("Ahhhhh qu'est ce qu'on est serrés, au fond de cette boîte... (but nobody understands, he is french)");
                    }
                    break;
                case 2 :
                    if(!animal_list.isEmpty())
                    {
                        ((Animal)(Findvictim(animal_list))).defecate();
                    }
                    break;
                case 3 : 
                    if(!animal_list.isEmpty() && !teenagers_list.isEmpty())
                    {
                        ((Monkey)(Findvictim(Monkey.get_monkey_list(animal_list)))).have_sex((Human)Findvictim(teenagers_list));
                    }
                    break;
                default :
                    write("The party is so intense",true);
            }
        }
        //+1 rage_state if someone is 100% drunk (only once)
        int compt = 0;
        if(!teenagers_list.isEmpty())
        {
            for(int p=0;p<teenagers_list.size();p++)
            {
                if(teenagers_list.get(p).drunk_state > 1 && compt == 0)
                {
                    teenagers_list.get(p).rage_state++;
                    compt = 1;
                }
            }
        }
    }

    /** RETURNS THE ELEMENT AND THE RANK OF THE ELEMENT WHICH HAS THE HIGHER rage_state IN THE LIST
     *
     * @param human_list
     * @return
     */
        public static ArrayList<Integer> Find_max_taux_enervement(ArrayList<Human> human_list)
    {
        int max = human_list.get(0).rage_state;
        int indice = 0;
        for(int j=0;j<human_list.size();j++)
        {
            if(max < human_list.get(j).rage_state)
            {
                max = human_list.get(j).rage_state;
                indice = j;
            }
        }
        ArrayList<Integer> maxind = new ArrayList();
        maxind.add(max);
        maxind.add(indice);
        return(maxind);
    }
    
    /** RETURNS THE RANK OF THE ELEMENT WHICH HAS THE HIGHER drunk_state IN THE LIST
     *
     * @param human_list
     * @return
     */
        public static Integer Find_max_degre_alcoolemie(ArrayList<Human> human_list)
    {
        float max = human_list.get(0).drunk_state;
        int indice = 0;
        for(int j=0;j<human_list.size();j++)
        {
            if(max < human_list.get(j).drunk_state)
            {
                max = human_list.get(j).drunk_state;
                indice = j;
            }
        }
        return(indice);
    }
 
    /** THIS METHOD RETURNS A RANDOM ELEMENT TAKEN IN THE LISTS IN ARGUMENTS
     *
     * @param listes
     * @return
     */
        public static Object Findvictim(ArrayList... listes) 
    {
        ArrayList big_list = new ArrayList();
        for(ArrayList liste : listes)
        {
            big_list.addAll(liste);
        }
        int pick1 = (int)(Math.random() * big_list.size());
        Object victime = big_list.get(pick1);
        return victime;
    }
 
    /** RETURNS A LIST WICH CONTAINS ALL THE HUMANS OF THE LIST THAT DISLIKE THE SONG ENTERED IN ARGUMENT
     *
     * @param music
     * @param listes
     * @return
     */
        public static ArrayList<Human> dislikers_music(Music music, ArrayList<Human>... listes) 
    {
        ArrayList<Human> dislikers = new ArrayList();
        for(ArrayList<Human> liste : listes)
        {
            for (int i= 0;i<liste.size();i++)
            {
                if(liste.get(i).hated_music == music)
                {
                    dislikers.add(liste.get(i));
                }
            }
        }
        return(dislikers);
    }
 
    /** INCREASE THE rage_state OF EVERY ELEMENT OF THE LIST ENTERED IN ARGUMENT
     *
     * @param liste
     */
        public static void up_enervement(ArrayList<Human> liste) 
    {
        for(int i=0;i<liste.size();i++)
        {
            liste.get(i).rage_state++;
        }
    }

    /** INCREASE THE drunk_state OF EVERY ELEMENT OF THE LIST ENTERED IN ARGUMENT
     *
     * @param listes
     */
        public static void all_drink(ArrayList<Object>... listes)
    {
        for(ArrayList<Object> liste : listes)
        {
            for(int i=0;i<liste.size();i++)
            {
                switch(liste.get(i).getClass().getSimpleName())
                {
                    case "Monkey" :
                        Monkey singe = (Monkey)liste.get(i);
                        singe.drink();
                        break;
                    case "Roomate" : 
                        Roomate coloc = (Roomate)liste.get(i);
                        coloc.drink();
                        break;
                    case "Guest" : 
                        Guest invite = (Guest)liste.get(i);
                        invite.drink();
                        break;
                }
            }
        }
        write("Round of drinks. Alcohol flows abundantly!",true);
    }
}
