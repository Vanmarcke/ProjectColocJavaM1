package projectcoloc;

/** Kinds of music that can be played during the party
 *
 * @author AFK
 */
public enum Music {

    ROCK, CLASSIC, TECHNO, GOSPEL, RAP, NO_MUSIC
}

