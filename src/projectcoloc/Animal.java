package projectcoloc;

import java.util.ArrayList;
import static projectcoloc.ProjectColoc.write; // Allow an animal function to write on the log

/**
 * The Animal class
 * @author Romain
 */
public abstract class Animal {

    /**
     * Name of the animal
     */
    public final String name;

    /**
     * Constructor without arguments, takes predefinied names
     */
    public Animal()
    {
        if (null != this.getClass().toString()) 
        switch (this.getClass().toString()) {
            case "class projectcoloc.Chat":
                this.name = "Felix";
                break;
            case "class projectcoloc.Lion":
                this.name="Simba";
                break;
            case "class projectcoloc.Singe":
                this.name="Mungo";
                break;
            default:
                this.name="Unknown";
                break;
        }
        else
        {
            this.name = "Unknow";
        }
    }
    
    /**
     * Constructor that can be used in a generate function
     * @param n number to identify the animal
     */
    public Animal(int n)
    {
        if ("Cat".equals(this.getClass().toString())) 
        {
            this.name = "Cat n°"+Integer.toString(n);
        }
        else if ("Lion".equals(this.getClass().toString()))
        {
            this.name="Lion n°"+Integer.toString(n);
        }
        else if ("Monkey".equals(this.getClass().toString()))
        {
            this.name="Monkey n°"+Integer.toString(n);
        }
        else
        {
            this.name="Unknown";
        }
    }
    
    /**
     * Constructor 
     * @param name of the animal
     */
    public Animal(String name)
    {
        this.name=name;
    }
    
    /**
     * Make an animal eat 
     */
    public void eat() {
        write("The "+this.getClass().getSimpleName()+" "+this.name+" eats.",true);
    }

    /**
     * Make an animal defecate
     */
    public void defecate() {
        write("The "+this.getClass().getSimpleName()+" "+this.name+" defecates.",true);
        write("Hopefully nobody cares...",true);
    }
    
    /**
     * Method that remove a dead animal from one or severall lists
     * usefull to avoid dead animals to perform an action
     * @param listes
     */
    public void is_dead(ArrayList... listes)
    {  
        ProjectColoc.write(this.name+" is dead.", true);
        for(ArrayList liste : listes)
        {
            if(liste.contains(this))
            {
                liste.remove(this);
            }
        }
    }
    
    /**
     * Returns a string which discribe the animal
     */
    @Override
    public String toString()
    {
        if (this instanceof Monkey) {
            Monkey monkey = (Monkey)this;
            return("the " + monkey.getClass().getSimpleName() + " " + monkey.name + 
                    ((monkey.drunk_state > 0.3f)?(", The teenagers forced him to drink, what an irresponsable behaviour !"):""));
        }
        else {
            return("the " + this.getClass().getSimpleName() + " " + this.name);
        }
    }
    
    
}
