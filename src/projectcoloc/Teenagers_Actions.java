package projectcoloc;


/** An interface to call methods in Roomate, Guest and Monkey classes
 * The methods are described in the classes
 * @author AFK
 */
public interface Teenagers_Actions {

    public void drink();
    
    public void pee(Object object);

    public void have_sex(Human humain);
    
    public void have_sex(Monkey singe);
}
