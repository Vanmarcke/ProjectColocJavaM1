package projectcoloc;

import java.util.ArrayList;
import java.util.Scanner;
import static projectcoloc.ProjectColoc.write;

public class Roomate extends Human implements Teenagers_Actions {
    
    /*
    CONSTRUCTORS
    */
    public Roomate(String psurnom, String pprenom, String psexe, int proom) {
        super(psurnom,pprenom,psexe);
        room = proom;
    }
    
    public Roomate(String psurnom, String pprenom, String psexe, float pdegre, int proom) {
        super(psurnom,pprenom,psexe,pdegre);
        room = proom;
    }
    
    public Roomate(String psurnom, String pprenom, String psexe, int pPV, float pdegre, int proom) {
        super(psurnom,pprenom,psexe, pPV,pdegre);
        room = proom;
    }
        
    public Roomate(String psurnom, String pprenom, String psexe, int pPV, Music pmusic, int proom) {
        super(psurnom,pprenom,psexe, pPV,pmusic);
        room = proom;
    }

    /** number of a roomate's room
     *
     */
    public int room;

    /**If a roomate has to pay the rent to the owner for example
     * 
     */
    public void pay(Owner owner) {
        write(this.first_name+" gives the money to "+owner.first_name,true);
    }

    /**This method makes a roomate drink
     *This increases his drunk_state
     */
    public void drink() {
        int pick1 = (int)(Math.random()*4);
        float alcool_drunk ;
        switch(pick1)
        {
            case 0 :
                alcool_drunk = 0.1f;
                break;
            case 1 : 
                alcool_drunk = 0.2f;
                break;
            case 2 :
                alcool_drunk = 0.3f;
                break;
            case 3 : 
                alcool_drunk = 0.4f;
                break;
            default :
                alcool_drunk = 0.05f;
        }
        this.drunk_state= this.drunk_state+alcool_drunk;
    }

    /**A roomate can pee on an other object (animal or human)
     * This generates an other action depending on the victim class
     * @param object
     */
    public void pee(Object object)
    {
        switch(object.getClass().getSimpleName())
        {
            case "Monkey" :
                Monkey singe = (Monkey)object;
                write(this.nickname+" pees on "+singe.name,true);
                singe.fights(this);
                break;
            case "Lion" :
                Lion lion = (Lion)object;
                write(this.nickname+" pees on "+lion.name,true);
                lion.roar();
                break;
            case "Cat" :
                Cat chat = (Cat)object;
                write(this.nickname+" pees on "+chat.name,true);
                chat.scratch(this);
                break;
            case "Roomate" :
                Roomate colocataire = (Roomate)object;
                write(this.nickname+" pees on "+colocataire.nickname,true);
                colocataire.rage_state++;
                break;
            case "Guest" :
                Guest invite = (Guest)object;
                write(this.nickname+" pees on "+invite.nickname,true);
                invite.rage_state++;
                break;
            case "Neighbour" :
                Neighbour voisin = (Neighbour)object;
                write(this.nickname+" pees on "+voisin.nickname,true);
                voisin.rage_state++;
                break;
            case "Owner" :
                Owner proprio = (Owner)object;
                write(this.nickname+" pees on "+proprio.nickname,true);
                proprio.rage_state++;
                break;
        }
    }
 
    /** A roomate can have sex with an other human of course
     *
     * @param humain
     */
    public void have_sex(Human humain) {
        write(this.first_name+" has good time with "+humain.nickname,true);
    }
    
    /** A roomate can also have sex with a monkey (crazy party) but his hp decrease
     *  
     * @param singe
     */
    public void have_sex(Monkey singe) {
        write(this.first_name+" has good time with "+singe.name+". Love is strange!",true);
        this.hp = this.hp-10;
    }
    
    /** This method generates a roomate, entering his nickname, name and sexe
     *
     * @param liste
     */
    public static void generate(ArrayList<Roomate> liste) {
        Scanner reader = new Scanner(System.in);  
        System.out.println("Enter a roomate name : ");
        String prenom = reader.next();
        System.out.println("Enter his nickname : ");
        String surnom = reader.next();
        System.out.println("Enter 'Man' or 'Woman' : ");
        String sexe = reader.next();
        int nbr = prenom.length();
        Roomate nouveau = new Roomate(surnom, prenom, sexe, nbr);
        liste.add(nouveau);
    }
    
    /** A roomate can enter in an ethylic_coma, this kills him (hp = 0)
     *
     * @param dead_list
     * @param listes
     */
    public void ethylic_coma(ArrayList dead_list,ArrayList... listes)
    {
        write(this.nickname+" enters in an ethylic coma.",true);
        this.hp=0;
        this.is_dead(dead_list, listes);
    }
}
