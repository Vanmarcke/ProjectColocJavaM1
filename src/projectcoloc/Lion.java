package projectcoloc;

import java.util.ArrayList;
import java.util.Scanner;
import static projectcoloc.ProjectColoc.write;

/**
 * The Lion Class, subclass of the Animal Class
 * @author Romain
 */
public class Lion extends Animal {

    /**
     * Contructor that calls the Animal Constructor
     */
    public Lion()
    {
        super();
    }
    
    /**
     * Contructor that calls the Animal Constructor
     * @param n
     */
    public Lion(int n)
    {
        super(n);
    }
    
    /**
     * Contructor that calls the Animal Constructor
     * @param name
     */
    public Lion(String name)
    {
        super(name);
    }
    
    /**
     * A Lion can roar
     */
    public void roar() {
        write("The lion "+this.name+" is roaring !\nWhat the fuck ??? WHO BRINGS A LION HERE ??",true);
    }

    /**
     * A Lion can devor a Human
     * @param humain
     */
    public void devour(Human humain) {
        write("The lion "+this.name+" was well fed !\n"+humain.first_name+" did not make it.",true);
        humain.hp=0;
    }
    
    /**
     * Method to generate a Lion and add it to the given list
     * @param liste
     */
    public static void generate(ArrayList<Lion> liste) {
        Scanner reader = new Scanner(System.in);  
        System.out.println("Enter his nickname : ");
        String surnom = reader.next();
        Lion nouveau = new Lion(surnom);
        liste.add(nouveau);
    }
}