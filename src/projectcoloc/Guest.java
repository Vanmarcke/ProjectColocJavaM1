package projectcoloc;
import java.util.Scanner; 
import java.util.ArrayList;
import static projectcoloc.ProjectColoc.up_enervement;
import static projectcoloc.ProjectColoc.write;

/**
 * The Guest class, subclass of Human class, implements the Teenagers_Actions interface
 * @author Romain
 */
public class Guest extends Human implements Teenagers_Actions {
    
    /**
     * Constructor
     * @param psurnom 
     * @param pprenom
     * @param psexe
     */
    public Guest(String psurnom, String pprenom, String psexe) {
        super(psurnom,pprenom,psexe);
    }
    
    /**
     * Constructor
     * @param psurnom
     * @param pprenom
     * @param psexe
     * @param pdegre
     */
    public Guest(String psurnom, String pprenom, String psexe, float pdegre) {
        super(psurnom,pprenom,psexe,pdegre);
    }
    
    /**
     * Constructor
     * @param psurnom
     * @param pprenom
     * @param psexe
     * @param pPV
     * @param pdegre
     */
    public Guest(String psurnom, String pprenom, String psexe, int pPV, float pdegre) {
        super(psurnom,pprenom,psexe, pPV,pdegre);
    }
        
    /**
     * Constructor
     * @param psurnom nickname 
     * @param pprenom first name
     * @param psexe sexe (Men or Women)
     * @param pPV heart points
     * @param pmusic music hated
     */
    public Guest(String psurnom, String pprenom, String psexe, int pPV, Music pmusic) {
        super(psurnom,pprenom,psexe, pPV,pmusic);
    }

    /**
     * A Guest can break something, this would rise the rage state of every students
     * @param teenagers_list
     */
    public void break_object(ArrayList teenagers_list) {
        up_enervement(teenagers_list);
    }


    /**
     * A Guest can drink, this will rise his drunk_state up to +40%
     */
    public void drink() {
        int pick1 = (int)(Math.random() * 4);
        float alcool_drunk ;
        switch(pick1)
        {
            case 0 :
                alcool_drunk = 0.1f;
                break;
            case 1 : 
                alcool_drunk = 0.2f;
                break;
            case 2 :
                alcool_drunk = 0.3f;
                break;
            case 3 : 
                alcool_drunk = 0.4f;
                break;
            default :
                alcool_drunk = 0f;
            
        }
        this.drunk_state= this.drunk_state+alcool_drunk;
    }
    
    /**
     * A Guest can pee on a Human or on a Animal
     * @param object
     */
    public void pee(Object object)
    {
        switch(object.getClass().getSimpleName())
        {
            case "Monkey" :
                Monkey singe = (Monkey)object;
                write(this.nickname+" has pissed on "+singe.name,true);
                singe.fights(this);
                break;
            case "Lion" :
                Lion lion = (Lion)object;
                write(this.nickname+" has pissed on "+lion.name,true);
                lion.roar();
                break;
            case "Cat" :
                Cat chat = (Cat)object;
                write(this.nickname+" has pissed on "+chat.name,true);
                chat.scratch(this);
                break;
            case "Roomate" :
                Roomate colocataire = (Roomate)object;
                write(this.nickname+" has pissed on "+colocataire.nickname,true);
                colocataire.rage_state++;
                break;
            case "Guest" :
                Guest invite = (Guest)object;
                write(this.nickname+" has pissed on "+invite.nickname,true);
                invite.rage_state++;
                break;
            case "Neighbour" :
                Neighbour voisin = (Neighbour)object;
                write(this.nickname+" has pissed on "+voisin.nickname,true);
                voisin.rage_state++;
                break;
            case "Owner" :
                Owner proprio = (Owner)object;
                write(this.nickname+" has pissed on "+proprio.nickname,true);
                proprio.rage_state++;
                break;
        }
    }

    /**
     * A Guest can have sex with a Human
     * @param humain
     */
    
    public void have_sex(Human humain) {
        write(this.first_name+" has sex with "+humain.nickname,true);
    }
    
    /**
     * A Guest can have sex with a Monkey
     * @param singe
     */
    public void have_sex(Monkey singe) {
        write(this.first_name+" has sex with "+singe.name,true);
    }
    
    /**
     * Method to generate a Guest, and add it to a given list
     * @param liste
     */
    public static void generate(ArrayList<Guest> liste) {
        Scanner reader = new Scanner(System.in);  
        System.out.println("Enter a guest's firstname : ");
        String prenom = reader.next();
        System.out.println("Enter his/her nickname : ");
        String surnom = reader.next();
        System.out.println("Enter 'Man' or 'Woman' : ");
        String sexe = reader.next();
        Guest nouveau = new Guest(surnom, prenom, sexe);
        liste.add(nouveau);
    }
    
    /**
     * A Guest can enter in an ethylic coma if he drunk too much
     * @param dead_list add it in the dead_list
     * @param listes remove it from one or severall lists
     */
    public void ethylic_coma(ArrayList dead_list,ArrayList... listes)
    {
        write(this.nickname+" enters in an ethylic coma.",true);
        this.hp=0;
        this.is_dead(dead_list, listes);
    }
}
