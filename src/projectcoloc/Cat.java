package projectcoloc;

import java.util.ArrayList;
import java.util.Scanner;
import static projectcoloc.ProjectColoc.write; // Allow an cat function to write on the log

/**
 * The class Cat, subclass of Animal
 * @author Romain
 */
public class Cat extends Animal {

    /**
     * Constructor that calls the animal constructor
     */
    public Cat()
    {
       super();
    }
    
    /**
     * Constructor that calls the animal constructor
     * @param n
     */
    public Cat(int n)
    {
        super(n);
    }
    
    /**
     * Constructor that calls the animal constructor
     * @param name
     */
    public Cat(String name)
    {
        super(name);
    }
    
    /**
     * A cat can scratch a Human person
     * @param humain
     */
    public void scratch(Human humain) {
        humain.hp=humain.hp-20;
        write("The cat "+this.name+" scratched "+humain.nickname+" !",true);
    }
    
    /**
     * The method to generate one or severall cats and add them to a given list
     * @param liste 
     */
    public static void generate(ArrayList<Cat> liste) {
        Scanner reader = new Scanner(System.in);  
        System.out.println("Enter its nickname : ");
        String surnom = reader.next();
        Cat nouveau = new Cat(surnom);
        liste.add(nouveau);
    }
}
