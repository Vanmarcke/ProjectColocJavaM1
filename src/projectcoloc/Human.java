package projectcoloc;

import java.util.ArrayList;
import static projectcoloc.ProjectColoc.write;

/**
 * The Human Class
 * @author Romain
 */
public abstract class Human {

    /**
     * nickname of a human          
     */
    public String nickname;

    /**
     * first name of a human, cannot be changed once initialized
     */
    public final String first_name;
    
    /**
     * the level of drunkenness a human is
     */
    public float drunk_state;

    /**
     * sexe of the human
     */
    public String sexe;
    
    /**
     * the hearts points the human still has
     */
    public int hp = 100;

    /**
     * the music he hates the most
     */
    public Music hated_music;
    
    /**
     * the level of wrath the human attains
     */
    public int rage_state = 0 ;
  
    /**
     * Constructor
     */
    public Human() {
        first_name = "Jean-Michel";
    }
    
    /**
     * Constructor
     * @param psurnom
     * @param pprenom
     * @param psexe
     */
    public Human(String psurnom, String pprenom, String psexe) {
        nickname = psurnom;
        first_name = pprenom;
        sexe = psexe;
    }
    
    /**
     * Constructor
     * @param psurnom
     * @param pprenom
     * @param psexe
     * @param pdegre
     */
    public Human(String psurnom, String pprenom, String psexe, float pdegre) {
        nickname = psurnom;
        first_name = pprenom;
        sexe = psexe;
        drunk_state = pdegre;
    }
    
    /**
     * Constructor
     * @param psurnom
     * @param pprenom
     * @param psexe
     * @param pPV
     * @param pdegre
     */
    public Human(String psurnom, String pprenom, String psexe, int pPV, float pdegre) {
        nickname = psurnom;
        first_name = pprenom;
        sexe = psexe;
        hp = pPV;
        drunk_state = pdegre;
    }
    
    /**
     * Constructor
     * @param psurnom
     * @param pprenom
     * @param psexe
     * @param pPV
     * @param pmusic
     */
    public Human(String psurnom, String pprenom, String psexe, int pPV, Music pmusic) {
        nickname = psurnom;
        first_name = pprenom;
        sexe = psexe;
        hated_music = pmusic;
        hp = pPV;
    }

    /**
     * Make a human talk
     * @param text
     */
    public void talk(String text) {
        write(this.first_name + " said: " + text,true);
    }

    /**
     * Make a human eats Potatoes, and say bullshit
     */
    public void eat_potatoes() {
        write(this.first_name + " eats potatoes.",true);
        this.talk("Potatoes will raise an empire and control the world. ("+this.first_name+" seems to be so high)");
    }
    
    /**
     * A Human can kill an animal
     * @param animal
     */
    public void kill(Animal animal){
        ProjectColoc.write(this.first_name + " kills "+animal.name,true);
    }
    
    /**
     * A Human can kill an other Human
     * @param humain
     */
    public void kill(Human humain){
        ProjectColoc.write(this.first_name + " kills "+humain.first_name,true);
        humain.hp = 0;
    }
    
    /**
     * A Human can fight with an Animal or a Human
     * @param object
     * @param dead_list
     * @param listes
     */
    public void fightswith(Object object, ArrayList dead_list, ArrayList... listes)
    {
        if(object == (Object)this)
        {
            switch(this.sexe)
            {
                case "Man" : 
                    write(this.first_name+" starts fighting... alone! ",true);
                    write("To calm down him, everybody tells "+this.first_name+" to eat potatoes.",true);
                    this.eat_potatoes();
                    this.hp = this.hp-30;
                    this.is_dead(dead_list, listes);
                    break;
                case "Woman" : 
                    write(this.first_name+" starts fighting... alone! ",true);
                    write("To calm down her, everybody tells "+this.first_name+" to eat potatoes.",true);
                    this.eat_potatoes();
                    this.hp = this.hp-30;
                    this.is_dead(dead_list, listes);
                    break;
            }
        }
        else
        {
            switch(object.getClass().getSimpleName())
            {
                case "Human" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Human)object).first_name, true);
                    this.hp = this.hp-30;
                    ((Human)object).hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    ((Human)object).is_dead(dead_list,listes);
                    break;
                case "Roomate" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Roomate)object).first_name, true);
                    this.hp = this.hp-30;
                    ((Roomate)object).hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    ((Roomate)object).is_dead(dead_list,listes);
                    break;
                case "Guest" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Guest)object).first_name, true);
                    this.hp = this.hp-30;
                    ((Guest)object).hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    ((Guest)object).is_dead(dead_list,listes);
                    break;
                case "Neighbour" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Neighbour)object).first_name, true);
                    this.hp = this.hp-30;
                    ((Neighbour)object).hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    ((Neighbour)object).is_dead(dead_list,listes);
                    break;
                case "Owner" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Owner)object).first_name, true);
                    this.hp = this.hp-30;
                    ((Owner)object).hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    ((Owner)object).is_dead(dead_list,listes);
                    break;
                case "Animal" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Animal)object).name, true);
                    this.hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    break;
                case "Lion" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Lion)object).name, true);
                    this.hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    break;
                case "Cat" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Cat)object).name, true);
                    this.hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    break;
                case "Monkey" :
                    ProjectColoc.write(this.first_name + " is fighting with " +((Monkey)object).name, true);
                    this.hp = this.hp-30;
                    this.is_dead(dead_list,listes);
                    break;
            }
        }   
    }
    
    /**
     * A Human can insult an other Human
     * @param humain
     */
    public void insult(Human humain)
    {
        if((Human)this == humain)
        {
            write(this.first_name+" insults the door.. ",true);
        }
        else
        {
            write(this.first_name + " insults "+humain.nickname, true);
            humain.rage_state++;
        }
    }
    
    /**
     * A methof to check if the current Human is dead or not 
     * @param dead_list if yes, add him to the dead list
     * @param listes and remove it from the given lists
     */
    public void is_dead(ArrayList dead_list,ArrayList... listes)
    {
        if(this.hp <= 0)
        {
            write(this.nickname+" is dead.", true);
            for(ArrayList liste : listes)
            {
                if(liste.contains(this))
                {
                    liste.remove(this);   
                }
            }
            dead_list.add(this);
        }
        else
        {
            ProjectColoc.write(this.nickname+" has "+this.hp+" HP.", true);
        }

    }
    
    /**
     * A Human can kill himself by jumping through the window
     * @param deadlist
     * @param listes
     */
    public void jump_out_of_window(ArrayList deadlist, ArrayList... listes)
    {
        this.hp = 0;
        ProjectColoc.write(this.nickname+" was too drunk, he jumped out of a window... RIP", true);
        this.is_dead(deadlist,listes);
    }
    
    @Override
    public String toString() {
        return (((this.hp < 1) ? ("† "): "") + this.first_name +
                ((this.sexe.equals("Woman")|| this.sexe.equals("W")) ? " ♀ ":" ♂ ")+
                "(alias: " + this.nickname + "" + ", class: "+
                this.getClass().getSimpleName() + ")"+
                ((this.hp < 1) ? (" does not belong to this world anymore. STATS: "): " STATS: HP: " + this.hp + " ||") + 
                " Rage state: " + this.rage_state + "/5" +
                ((this instanceof Roomate || this instanceof Guest) ? (" || Drunk state: "+
                ((this.drunk_state < 1f) ? (Math.round(this.drunk_state*100)+"%.") : ("100")+ "%.")):("."))
        );
    }
    
    
}