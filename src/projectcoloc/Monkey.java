package projectcoloc;

import java.util.ArrayList;
import java.util.Scanner;
import static projectcoloc.ProjectColoc.write;

/** The class monkey, a subclass of Animal
 *
 * @author AFK
 */
public class Monkey extends Animal implements Teenagers_Actions {

    float drunk_state ;
    
    /** A default constructor, drunk_state is set to 0
     *
     */
    public Monkey()
    {
        super();
        this.drunk_state = 0 ;
    }
    
    /**A constructor with an integer to give the monkey a default name
     * (for example Monkey(2) will create a monkey named 'Monkey n°2')
     * @param n
     */
    public Monkey(int n)
    {
        super(n);
        this.drunk_state = 0 ;
    }
    
    /**A constructor to give a name to the Monkey
     *
     * @param name
     */
    public Monkey(String name)
    {
        super(name);
        this.drunk_state = 0 ;
    }
    
    /** This method makes the monkey drink : it increases his alcool_drunk up
     *
     */
    public void drink() {
        int pick1 = (int)(Math.random() * 4);
        float alcool_drunk ;
        switch(pick1)
        {
            case 0 :
                alcool_drunk = 0.1f;
                break;
            case 1 : 
                alcool_drunk = 0.2f;
                break;
            case 2 :
                alcool_drunk = 0.3f;
                break;
            case 3 : 
                alcool_drunk = 0.4f;
                break;
            default :
                alcool_drunk = 0f;
            
        }
        this.drunk_state= this.drunk_state+alcool_drunk;
    }
    
    /** A monkey can pee on an object (Human or Animal). 
     * According to the case, this generates different actions
     * @param object
     */
    public void pee(Object object)
    {
        switch(object.getClass().getSimpleName())
        {
            case "Monkey" :
                Monkey singe = (Monkey)object;
                write(this.name+" has pissed on "+singe.name,true);
                singe.fights(this);
                break;
            case "Lion" :
                Lion lion = (Lion)object;
                write(this.name+" has pissed on "+lion.name,true);
                lion.roar();
                break;
            case "Cat" :
                Cat chat = (Cat)object;
                write(this.name+" has pissed on "+chat.name,true);
                break;
            case "Roomate" :
                Roomate colocataire = (Roomate)object;
                write(this.name+" has pissed on "+colocataire.nickname,true);
                colocataire.rage_state++;
                break;
            case "Guest" :
                Guest invite = (Guest)object;
                write(this.name+" has pissed on "+invite.nickname,true);
                invite.rage_state++;
                break;
            case "Neighbour" :
                Neighbour voisin = (Neighbour)object;
                write(this.name+" has pissed on "+voisin.nickname,true);
                voisin.rage_state++;
                break;
            case "Owner" :
                Owner proprio = (Owner)object;
                write(this.name+" has pissed on "+proprio.nickname,true);
                proprio.rage_state++;
                break;
        }
    }


    /** A monkey can fight with a human : human hp will decrease
     *
     * @param humain
     */
    
    public void fights(Human humain) {
        write("The monkey "+this.name+" wants some human's blood tonight ! He's fighting with "+humain.nickname,true);
        humain.hp = humain.hp-30;
    }
    
    /** The monkey can fight with a monkey
     *
     * @param singe
     */
    public void fights(Monkey singe){
        write("The monkey "+this.name+" has a good fight with "+singe.name,true);
    }
    
    /**The monkey can have sex with a human (yeah it's a crazy party)
The human hp will be somewhat decreased
     * @param humain
     */
    public void have_sex(Human humain) {
        write("The monkey "+this.name+" has some good time with "+humain.nickname, true);
        humain.hp = humain.hp-10;
    }
    
    /**Of course a monkey can have sex with an other monkey
     *
     * @param singe
     */
    public void have_sex(Monkey singe) {
        write("Oh look, that's so cute ! two monkey " + this.name + " and " + singe.name + " fell in love together",true);
    }

    /** A monkey can steal an object to a human
     *
     * @param humain
     */
    public void steal(Human humain) {
        write("What a bad guy ! The monkey "+this.name+" has stolen something to "+humain.nickname,true);
        humain.rage_state++;
    }
    
    /** This method generates a monkey and add it to the existing monkey_list
     *
     * @param liste
     */
    public static void generate(ArrayList<Monkey> liste) {
        Scanner reader = new Scanner(System.in);  
        System.out.println("Enter a name : ");
        String surnom = reader.next();
        Monkey nouveau = new Monkey(surnom);
        liste.add(nouveau);
    }

    /** This method permits us to get all the monkeys from an other list (like animal_list)
     * The monkeys will be returned in an ArrayList
     * @param animal_list
     * @return
     */
        public static ArrayList<Monkey> get_monkey_list(ArrayList<Animal> animal_list)
    {
        ArrayList<Monkey> monkey_list = new ArrayList();
        for(int i = 0;i<animal_list.size();i++)
        {
            if(animal_list.get(i) instanceof Monkey)
            {
                monkey_list.add((Monkey)animal_list.get(i));
            }
        }
        return(monkey_list);
    }
 
    /**The monkey can enter in an ethylic coma
     * 
     */
    public void ethylic_coma()
    {
        write(this.name+" enters in an ethylic coma.",true);
    }
}
