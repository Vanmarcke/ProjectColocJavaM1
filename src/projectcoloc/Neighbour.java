package projectcoloc;

import java.util.ArrayList;
import java.util.Scanner;
import static projectcoloc.ProjectColoc.write;

/** The Neighbour class
 *
 * @author AFK
 */
public class Neighbour extends Human implements Adults_Actions {

    public boolean has_children;

    /** The Neighbour's class constructor
     *
     * @param surnom
     * @param prenom
     * @param sexe
     */
    public Neighbour(String surnom, String prenom, String sexe)
    {
        super(surnom,prenom,sexe);
    }
    
    /** A neighbour can complains to the members of the party and to the owner, depending on his rage_state
     *
     * @param owner
     * @param teenagers
     */
    public void complain(Owner owner, ArrayList teenagers) {
        this.knock();
        if(this.rage_state == 0 || this.rage_state == 1)
        {
            this.talk("Can you please stop the music, it's so noisy here.");
            if(this.has_children)
            {
                this.rage_state = this.rage_state+2;
            }
            else
            {
                this.rage_state++;
            }
        }
        else if(this.rage_state == 2)
        {
            this.talk("If you do not stop this NOW I will call the owner !");
            if(this.has_children)
            {
                this.rage_state = this.rage_state+2;
            }
            else
            {
                this.rage_state++;
            }
        }
        else if(this.rage_state == 3)
        {
            this.talk("CAN YOU STOP THIS F*CKING PARTY?? I CALLED THE OWNER !");
            if(this.has_children)
            {
                this.rage_state = this.rage_state+2;
            }
            else
            {
                this.rage_state++;
            }
            owner.rage_state++;
        }
        else
        {
            int i = (int)(Math.random() * teenagers.size());
            Human victim = (Human)teenagers.get(i);
            this.kill(victim);
        }
    }

    /** A neighbour can claim a money account to the roomates, if they break something for example
     *
     * @param n
     */
    public void claim(int n) {
       this.talk("You owe me "+Integer.toString(n)+".");
    }

    /** The neighbour can knock on the roomates door during the party
     *
     */
    public void knock() {
        write("The neighbour "+this.first_name+" is knocking on the door...",true);
    }
    
    /** Method used to generate Neighbour, with a name, nickname and entering his sexe
     *
     * @param liste
     */
    public static void generate(ArrayList<Neighbour> liste) {
        Scanner reader = new Scanner(System.in);  
        System.out.println("Enter a guest name : ");
        String prenom = reader.next();
        System.out.println("Enter his nickname : ");
        String surnom = reader.next();
        System.out.println("Enter 'Man' or 'Woman' : ");
        String sexe = reader.next();
        Neighbour nouveau = new Neighbour(surnom, prenom, sexe);
        liste.add(nouveau);
    }
}
