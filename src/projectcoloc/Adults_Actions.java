package projectcoloc;

/**
 * An interface to call methods in Owner and Neighbour classes
 * The methods are described in the classes
 * @author Romain
 */
public interface Adults_Actions {

    public void claim(int n);

    public void knock();
}
